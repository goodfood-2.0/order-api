﻿using DAL.Entity;

namespace BLL.Interfaces
{
    public interface IOrderContentsService
    {
        public Task<IEnumerable<OrderContents>> GetOrderContentsAsync();
        public Task<OrderContents> GetOrderContentsByIdAsync(int id);
        public Task CreateOrderContentsAsync(OrderContents OrdersContents);
        public Task UpdateOrderContentsAsync(int id, OrderContents OrdersContents);
        public Task DeleteOrderContentsAsync(int id);
        public Task<IEnumerable<TopBestContent>> GetOrderBestContents(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<IEnumerable<TopSoldContent>> GetOrderBestSold(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<TotalAmount> GetTotalAmount(int IdRestaurant, DateTime dateMin, DateTime dateMax);
    }
}
