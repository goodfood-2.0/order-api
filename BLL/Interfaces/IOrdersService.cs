﻿using DAL.Entity;

namespace BLL.Interfaces
{
    public interface IOrdersService
    {
        public Task<IEnumerable<Orders>> GetOrdersAsync();
        public Task<Orders> GetOrdersByIdAsync(int id);
        public Task CreateOrdersAsync(Orders Orders);
        public Task UpdateOrdersAsync(int id, Orders Orders);
        public Task DeleteOrdersAsync(int id);
        public Task<IEnumerable<Orders>> GetUserOrdersAsync(string email);
        public Task<CountOrders> CountOrders(int IdRestaurant, DateTime dateMin, DateTime dateMax);
    }
}
