﻿using DAL.Entity;

namespace BLL.Interfaces
{
    public interface IBasketsServices
    {
        Task<IEnumerable<Baskets>> GetBasketsAsync();
        Task<Baskets> GetBasketsByUserIdAsync(string id);
        Task CreateBasketsAsync(Baskets OrdersContents);
        Task DeleteBasketsAsync(string id);
        Task UpdateBasketsAsync(string id, Baskets baskets);
        Task<Orders> Validate(Orders myOrder, string id);
    }
}
