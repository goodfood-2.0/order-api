﻿using BLL.Interfaces;
using DAL.Entity;
using DAL.Interfaces;

namespace BLL.Services
{
    public class OrderContentsService : IOrderContentsService
    {
        public IOrderContentsOperation _OrdersContentsOperation;

        public OrderContentsService(IOrderContentsOperation OrdersContentsOperation)
        {
            _OrdersContentsOperation = OrdersContentsOperation;
        }

        public async Task<IEnumerable<OrderContents>> GetOrderContentsAsync()
        {
            return await _OrdersContentsOperation.GetOrderContentsAsync();
        }

        public async Task<OrderContents> GetOrderContentsByIdAsync(int id)
        {
            return await _OrdersContentsOperation.GetOrderContentsByIdAsync(id);
        }

        public async Task<IEnumerable<TopBestContent>> GetOrderBestContents(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _OrdersContentsOperation.GetOrderBestContents(NumberOfBest, IdRestaurant, dateMin, dateMax);
        }

        public async Task<IEnumerable<TopSoldContent>> GetOrderBestSold(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _OrdersContentsOperation.GetOrderBestSold(NumberOfBest, IdRestaurant, dateMin, dateMax);
        }

        public async Task<TotalAmount> GetTotalAmount(int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _OrdersContentsOperation.GetTotalAmount(IdRestaurant, dateMin, dateMax);
        }


        public async Task CreateOrderContentsAsync(OrderContents OrdersContents)
        {
            await _OrdersContentsOperation.CreateOrderContentsAsync(OrdersContents);
        }

        public async Task UpdateOrderContentsAsync(int id, OrderContents OrdersContents)
        {
            await _OrdersContentsOperation.UpdateOrderContentsAsync(id, OrdersContents);
        }

        public async Task DeleteOrderContentsAsync(int id)
        {
            await _OrdersContentsOperation.DeleteOrderContentsAsync(id);
        }
    }
}
