﻿using BLL.Interfaces;
using DAL.Entity;
using DAL.Interfaces;

namespace BLL.Services
{
    public class OrdersService : IOrdersService
    {
        public IOrdersOperation _OrdersOperation;

        public OrdersService(IOrdersOperation OrdersOperation)
        {
            _OrdersOperation = OrdersOperation;
        }

        public async Task<IEnumerable<Orders>> GetOrdersAsync()
        {
            return await _OrdersOperation.GetOrdersAsync();
        }

        public async Task<Orders> GetOrdersByIdAsync(int id)
        {
            return await _OrdersOperation.GetOrdersByIdAsync(id);
        }

        public async Task<IEnumerable<Orders>> GetUserOrdersAsync(string email)
        {
            return await _OrdersOperation.GetUserOrdersAsync(email);
        }


        public async Task<CountOrders> CountOrders(int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            return await _OrdersOperation.CountOrders(IdRestaurant, dateMin, dateMax);
        }

        public async Task CreateOrdersAsync(Orders Orders)
        {
            await _OrdersOperation.CreateOrdersAsync(Orders);
        }

        public async Task UpdateOrdersAsync(int id, Orders Orders)
        {
            await _OrdersOperation.UpdateOrdersAsync(id, Orders);
        }

        public async Task DeleteOrdersAsync(int id)
        {
            await _OrdersOperation.DeleteOrdersAsync(id);
        }
    }
}
