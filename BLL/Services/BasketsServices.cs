﻿using BLL.Interfaces;
using DAL.Entity;
using DAL.Interfaces;
using Newtonsoft.Json;
using System.Text;

namespace BLL.Services
{
    public class BasketsServices : IBasketsServices
    {
        public IBasketsOperation _basketsOperation;
        public IOrdersOperation _ordersOperation;
        private readonly HttpClient _catalogApi;
        private readonly HttpClient _restaurantApi;
        private readonly HttpClient _deliveryApi;


        public BasketsServices(IBasketsOperation basketsOperation, IOrdersOperation ordersOperation, IHttpClientFactory clientFactory)
        {
            _basketsOperation = basketsOperation;
            _ordersOperation = ordersOperation;
            _catalogApi = clientFactory.CreateClient("Catalog-api");
            _restaurantApi = clientFactory.CreateClient("Restaurant-api");
            _deliveryApi = clientFactory.CreateClient("Delivery-api");
        }

        public async Task<IEnumerable<Baskets>> GetBasketsAsync()
        {
            return await _basketsOperation.GetBasketsAsync();
        }

        public async Task<Baskets> GetBasketsByUserIdAsync(string id)
        {
            return await _basketsOperation.GetBasketsByUserIdAsync(id);
        }

        public async Task<Orders> Validate(Orders myOrder, string id)
        {
            Baskets myBasket = await _basketsOperation.GetBasketsByUserIdAsync(id);
            Console.WriteLine("myBasket");
            Console.WriteLine(JsonConvert.SerializeObject(myBasket));
            Console.WriteLine("myOrder");
            Console.WriteLine(JsonConvert.SerializeObject(myOrder));

            myOrder.OrderContents.Clear();
            List<string> stringlistids = new List<string>();
            if (myBasket.products.Count() == 0)
            {
                throw new Exception("No product in basket.");
            }

            foreach (var basketProduct in myBasket.products)
            {
                OrderContents orderContent = new OrderContents
                {
                    IdContent = basketProduct.IdContent,
                    Quantity = basketProduct.Quantity
                };
                myOrder.OrderContents.Add(orderContent);
                stringlistids.Add(orderContent.IdContent.ToString());
            }

            string commaSeparatedIds = string.Join(",", stringlistids);

            // PREPARE GET PRICE
            var urlCatalogProducts = $"/catalog/products?ids=" + commaSeparatedIds;
            try
            {
                var responseCatalogProducts = await _catalogApi.GetAsync(urlCatalogProducts);
                responseCatalogProducts.EnsureSuccessStatusCode();
                string jsonResponseCatalogProducts = await responseCatalogProducts.Content.ReadAsStringAsync();
                Console.WriteLine("jsonResponseCatalogProducts");
                Console.WriteLine(jsonResponseCatalogProducts);

                ApiResponse<Product> tempProducts = JsonConvert.DeserializeObject<ApiResponse<Product>>(jsonResponseCatalogProducts);

                foreach (var orderContent in myOrder.OrderContents)
                {
                    var matchingProduct = tempProducts.Data.FirstOrDefault(p => p.ID == orderContent.IdContent);
                    if (matchingProduct != null)
                    {
                        orderContent.Price = (decimal)matchingProduct.Price;
                    }
                    else
                    {
                        throw new Exception("No price found for a product in order, order content id is : " + orderContent.IdContent);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while trying to fetch catalog products, error : " + ex.Message, ex);
            }

            // PREPARE GET RELATIONS AND REQUIRED QTY
            var url = $"/catalog/product-ingredients?ids_product=" + commaSeparatedIds;
            string jsonResponse;
            try
            {
                var response = await _catalogApi.GetAsync(url);
                response.EnsureSuccessStatusCode();
                jsonResponse = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Error while trying to get product ingredients, error : " + ex.Message, ex);
            }
            ApiResponse<Ingredient> obj = JsonConvert.DeserializeObject<ApiResponse<Ingredient>>(jsonResponse)!;

            var payloadObj = obj.Data.Select(x =>
            {
                return new
                {
                    id_restaurant = myOrder.IdRestaurant,
                    id_ingredient = x.IngredientId,
                    quantity = x.RequiredQuantity * myOrder.OrderContents.Where(y => y.IdContent == x.ProductId).FirstOrDefault().Quantity,
                };
            });

            // EXECUTE STOCK TRANSACTION
            var urlRestaurant = $"/restaurant/stocks/transaction/remove";
            Console.WriteLine("payloadObj" + payloadObj);
            string payloadRestaurant = JsonConvert.SerializeObject(payloadObj);
            Console.WriteLine("payloadRestaurant" + payloadRestaurant);
            HttpContent content = new StringContent(payloadRestaurant, Encoding.UTF8, "application/json");

            try
            {
                var responseRestaurant = await _restaurantApi.PostAsync(urlRestaurant, content);
                responseRestaurant.EnsureSuccessStatusCode(); // This will throw if the response is not successful (status code >= 400)
            }
            catch (Exception ex)
            {
                throw new Exception("Error while trying decrement stock, error : " + ex.Message, ex);
            }

            // CREATE ORDER (SUCCESSFUL PREPARATION)
            myOrder.isValidate = false;
            myOrder = await _ordersOperation.CreateOrdersAsync(myOrder);

            // CREATE A DELIVERY
            var urlDelivery = $"/deliveries";

            Deliveries deli = new Deliveries();
            deli.Id_Restaurant = myOrder.IdRestaurant;
            deli.Id_Order = myOrder.Id;
            deli.Email = myOrder.Email;

            string payloadDelivery = JsonConvert.SerializeObject(deli);
            Console.WriteLine("payloadDelivery : " + payloadDelivery);
            HttpContent contentDelivery = new StringContent(payloadDelivery, Encoding.UTF8, "application/json");

            try
            {
                var responseDelivery = await _deliveryApi.PostAsync(urlDelivery, contentDelivery);
                responseDelivery.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                throw new Exception("Error while trying to create a delivery, error : " + ex.Message, ex);
            }

            // UPDATE ORDER TO VALIDATE THE FULL FLOW
            try
            {
                myOrder.isValidate = true;
                await _ordersOperation.UpdateOrdersAsync(myOrder.Id, myOrder);
                await DeleteBasketsAsync(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Error while trying to create or update an order, error : " + ex.Message, ex);
            }

            return myOrder;
        }

        public async Task CreateBasketsAsync(Baskets OrdersContents)
        {
            await _basketsOperation.CreateBasketsAsync(OrdersContents);
        }

        public async Task DeleteBasketsAsync(string id)
        {
            await _basketsOperation.DeleteBasketsAsync(id);
        }

        public async Task UpdateBasketsAsync(string id, Baskets baskets)
        {
            await _basketsOperation.UpdateBasketsAsync(id, baskets);
        }
    }

    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public bool Activated { get; set; }
        public string Image { get; set; }
        [JsonProperty("id_restaurant")]
        public string idRestaurant { get; set; }
    }

    public class Deliveries
    {
        public int StatusId { get; set; }
        public int? Id_Restaurant { get; set; }
        public int? Id_Order { get; set; }
        public string? Email { get; set; }
        public string? PicturePath { get; set; }
    }

    public class Ingredient
    {
        [JsonProperty("id_product")]
        public int ProductId { get; set; }

        [JsonProperty("id_ingredient")]
        public int IngredientId { get; set; }

        [JsonProperty("required_quantity")]
        public int RequiredQuantity { get; set; }
    }

    public class ApiResponse<T> where T : class
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("data")]
        public List<T> Data { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }
    }
}
