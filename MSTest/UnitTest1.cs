using DAL.Entity;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Text;

namespace MSTest
{
    [TestClass]
    public class UnitTest1
    {
        private static WebApplicationFactory<order_api.Program> _factory;
        private static HttpClient _client;
        private static HttpClient Client
        {
            get
            {
                if (_client == null)
                {
                    _client = _factory.CreateClient();
                    _client.DefaultRequestHeaders.Accept.Clear();
                    _client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                }
                return _client;
            }
        }

        public static async Task<bool> initBDD()
        {

            BasketsProducts basketsProducts = new BasketsProducts()
            {
                IdContent = 1,
                ContentName = "Banane",
                Quantity = 1
            };
            BasketsProducts basketsProducts1 = new BasketsProducts()
            {
                IdContent = 2,
                ContentName = "Chocolat",
                Quantity = 2
            };
            BasketsProducts basketsProducts2 = new BasketsProducts()
            {
                IdContent = 3,
                ContentName = "Burger",
                Quantity = 1
            };

            List<BasketsProducts> listBasketProd = new List<BasketsProducts>()
            {
                basketsProducts2,
                basketsProducts1,
                basketsProducts
            };

            Baskets content = new Baskets()
            {
                UserId = "user13",
                products = listBasketProd
            };



            var response = await Client.PostAsync("/Baskets", new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));

            var jsonResponse = await response.Content.ReadAsStringAsync();
            var baskets = JsonConvert.DeserializeObject<Baskets>(jsonResponse);

            basketsProducts = new BasketsProducts()
            {
                IdContent = 1,
                ContentName = "Prince",
                Quantity = 1
            };


            listBasketProd = new List<BasketsProducts>()
            {
                basketsProducts2,
                basketsProducts1,
                basketsProducts
            };

            content = new Baskets()
            {
                UserId = "user16",
                products = listBasketProd
            };



            var response3 = await Client.PostAsync("/Baskets", new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));

            var jsonResponse3 = await response.Content.ReadAsStringAsync();
            var baskets3 = JsonConvert.DeserializeObject<Baskets>(jsonResponse);




            OrderContents orderContents = new OrderContents()
            {
                Id = 1,
                ContentName = "Banane",
                IdContent = 1,
                Quantity = 1,
                IdOrder = 1,
                Price = 10
            };
            OrderContents orderContents1 = new OrderContents()
            {
                Id = 2,
                ContentName = "Pizza",
                IdContent = 2,
                Quantity = 2,
                IdOrder = 1,
                Price = 12
            };
            OrderContents orderContents2 = new OrderContents()
            {
                Id = 3,
                ContentName = "Chocolat",
                IdContent = 2,
                Quantity = 1,
                IdOrder = 3,
                Price = 13
            };

            List<OrderContents> ListOrder = new List<OrderContents>()
            {
                orderContents,
                orderContents1,
                orderContents2
            };

            Orders orders = new Orders()
            {
                Id = 0,
                City = "Test",
                AdditionnalAddress = "Test",
                Address = "Test",
                CommandType = "Utilisateur",
                Country = "Test",
                CreatedAt = DateTime.Now,
                OrderContents = ListOrder,
                isValidate = true,
                IdRestaurant = 0,
                Email = "Test.Test@patate.com",
                UpdatedAt = DateTime.Now,
                ZipCode = "76000"
            };




            var response2 = await Client.PostAsync("/Orders", new StringContent(JsonConvert.SerializeObject(orders), Encoding.UTF8, "application/json"));


            var jsonResponse2 = await response2.Content.ReadAsStringAsync();

            var order = JsonConvert.DeserializeObject<Orders>(jsonResponse2);

            return baskets.UserId != "" && order.Id != 0;
        }

        [ClassInitialize]
        public static async Task ClassInit(TestContext context)
        {
            _factory = new WebApplicationFactory<order_api.Program>();
            initBDD();
        }



        [TestMethod]
        public async Task GetCountOrdersByRestaurantId_ReturnsOkResult()
        {

            var response = await _client.GetAsync("/orders/restaurant/1/count?dateMin=2024-04-03T20%3A52%3A01.766Z&dateMax=2024-12-03T20%3A52%3A01.766Z");

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var deliveries = JsonConvert.DeserializeObject<CountOrders>(contentResponse);

            Assert.IsNotNull(deliveries);
        }


        [TestMethod]
        public async Task GetOrders_ReturnsOkResult()
        {
            var response = await _client.GetAsync("/orders");

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var order = JsonConvert.DeserializeObject<List<Orders>>(contentResponse);

            Assert.IsNotNull(order);
        }

        [TestMethod]
        public async Task GetOrdersById_ReturnsOkResult()
        {
            var response = await _client.GetAsync("/orders/2");

            var contentResponse = await response.Content.ReadAsStringAsync();
            var order = JsonConvert.DeserializeObject<Orders>(contentResponse);

            Assert.IsNotNull(order);
        }

        [TestMethod]
        public async Task PostOrdersById_ReturnsOkResult()
        {
            OrderContents orderContents = new OrderContents()
            {
                Id = 0,
                ContentName = "Chocolat au lait",
                IdContent = 2,
                Quantity = 1,
                IdOrder = 3,
                Price = 15
            };

            List<OrderContents> ListOrder = new List<OrderContents>()
            {
                orderContents,
            };

            Orders orders = new Orders()
            {
                Id = 0,
                City = "Test",
                AdditionnalAddress = "Test",
                Address = "Test",
                CommandType = "Utilisateur",
                Country = "Test",
                CreatedAt = DateTime.Now,
                OrderContents = ListOrder,
                isValidate = true,
                IdRestaurant = 0,
                Email = "Test.Test@patate.com",
                UpdatedAt = DateTime.Now,
                ZipCode = "76000"
            };

            var response = await _client.PostAsync("/orders", new StringContent(JsonConvert.SerializeObject(orders), Encoding.UTF8, "application/json"));

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var order = JsonConvert.DeserializeObject<Orders>(contentResponse);

            Assert.IsNotNull(order);
        }

        //[TestMethod]
        //public async Task DeleteOrders_ReturnsOkResult()
        //{
        //    var response = await _client.DeleteAsync("/orders/1");

        //    Assert.IsTrue(response.IsSuccessStatusCode);
        //}


        [TestMethod]
        public async Task GetordersByEmail_ReturnsOkResult()
        {
            var response = await _client.GetAsync("/orders/user?Email=Test.TEST@patate.com");

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var order = JsonConvert.DeserializeObject<List<Orders>>(contentResponse);

            Assert.IsNotNull(order);
        }

        [TestMethod]
        public async Task GetOrderContents_ReturnsOkResult()
        {

            var response = await _client.GetAsync("/ordercontents");

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var deliveries = JsonConvert.DeserializeObject<List<OrderContents>>(contentResponse);

            Assert.IsNotNull(deliveries);
        }


        [TestMethod]
        public async Task PostOrderContents_ReturnsOkResult()
        {

            OrderContents orderContents = new OrderContents()
            {
                Id = 0,
                ContentName = "Chocolat au lait",
                IdContent = 2,
                Quantity = 1,
                IdOrder = 3,
                Price = 15
            };

            var response = await _client.PostAsync("/ordercontents", new StringContent(JsonConvert.SerializeObject(orderContents), Encoding.UTF8, "application/json"));

            var contentResponse = await response.Content.ReadAsStringAsync();
            var deliveries = JsonConvert.DeserializeObject<OrderContents>(contentResponse);

            Assert.IsTrue(deliveries.ContentName == orderContents.ContentName, "Impossible de changer le status");
        }

        //[TestMethod]
        //public async Task GetOrderContentsById_ReturnsOkResult()
        //{
        //    await initBDD();

        //    var response = await _client.GetAsync("/ordercontents/2");

        //    var contentResponse = await response.Content.ReadAsStringAsync();
        //    var deliveries = JsonConvert.DeserializeObject<OrderContents>(contentResponse);

        //    Assert.IsNotNull(deliveries);
        //}

        [TestMethod]
        public async Task PutOrderContentsById_ReturnsOkResult()
        {

            OrderContents orderContents = new OrderContents()
            {
                Id = 2,
                ContentName = "Chocolat au lait",
                IdContent = 2,
                Quantity = 1,
                IdOrder = 3,
                Price = 15
            };


            var response = await _client.PutAsync("/ordercontents/2", new StringContent(JsonConvert.SerializeObject(orderContents), Encoding.UTF8, "application/json"));

            response.EnsureSuccessStatusCode();
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        //[TestMethod]
        //public async Task DeleteOrderContentsById_ReturnsOkResult()
        //{
        //    var response = await _client.DeleteAsync("/ordercontents/1");

        //    Assert.IsTrue(response.IsSuccessStatusCode);
        //}

        [TestMethod]
        public async Task GetOrderContentsBestContents_ReturnsOkResult()
        {
            var response = await _client.GetAsync("/ordercontents/bestcontents?NumberOfBest=5&IdRestaurant=1&dateMin=2024-04-03T21%3A13%3A59.075Z&dateMax=2024-12-03T21%3A13%3A59.075Z");

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var orderContents = JsonConvert.DeserializeObject<List<TopBestContent>>(contentResponse);

            Assert.IsNotNull(orderContents);
        }

        [TestMethod]
        public async Task GetOrderContentsBestSold_ReturnsOkResult()
        {
            var response = await _client.GetAsync("/ordercontents/bestsold?NumberOfBest=5&IdRestaurant=1&dateMin=2024-04-03T21%3A13%3A59.075Z&dateMax=2024-12-03T21%3A13%3A59.075Z");

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var orderContents = JsonConvert.DeserializeObject<List<TopSoldContent>>(contentResponse);

            Assert.IsNotNull(orderContents);
        }

        [TestMethod]
        public async Task GetOrderContentsTtotalAmount_ReturnsOkResult()
        {
            var response = await _client.GetAsync("/ordercontents/totalamount?NumberOfBest=5&IdRestaurant=1&dateMin=2024-04-03T21%3A13%3A59.075Z&dateMax=2024-12-03T21%3A13%3A59.075Z");

            response.EnsureSuccessStatusCode();

            var contentResponse = await response.Content.ReadAsStringAsync();
            var totalAmount = JsonConvert.DeserializeObject<TotalAmount>(contentResponse);

            Assert.IsNotNull(totalAmount);
        }

        [TestMethod]
        public async Task PutBaskets_ReturnsOkResult()
        {

            BasketsProducts basketsProducts = new BasketsProducts()
            {
                IdContent = 1,
                ContentName = "Pizza",
                Quantity = 2
            };
            BasketsProducts basketsProducts1 = new BasketsProducts()
            {
                IdContent = 2,
                ContentName = "Chocolat",
                Quantity = 2
            };
            BasketsProducts basketsProducts2 = new BasketsProducts()
            {
                IdContent = 3,
                ContentName = "Burger",
                Quantity = 1
            };

            List<BasketsProducts> listBasketProd = new List<BasketsProducts>()
            {
                basketsProducts2,
                basketsProducts1,
                basketsProducts
            };

            Baskets content = new Baskets()
            {
                UserId = "user13",
                products = listBasketProd
            };

            var response = await Client.PutAsync("/Baskets/" + content.UserId, new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));

            // Assert   
            Assert.IsTrue(response.IsSuccessStatusCode, "Impossible de Put");
        }

        [TestMethod]
        public async Task GetBaskets_ReturnsOkResult()
        {
            var response = await Client.GetAsync("/Baskets");

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode, "Impossible de Get");
        }

        [TestMethod]
        public async Task GetBasketsById_ReturnsOkResult()
        {
            //Assert.IsTrue(await initBDD(), "Error post");

            var response = await Client.GetAsync("/Baskets/user13");

            // Assert   
            Assert.IsTrue(response.IsSuccessStatusCode, "Impossible de Get");
        }

        [TestMethod]
        public async Task PostBaskets_ReturnsOkResult()
        {
            BasketsProducts basketsProducts = new BasketsProducts()
            {
                IdContent = 1,
                ContentName = "Banane",
                Quantity = 1
            };
            BasketsProducts basketsProducts1 = new BasketsProducts()
            {
                IdContent = 2,
                ContentName = "Chocolat",
                Quantity = 2
            };
            BasketsProducts basketsProducts2 = new BasketsProducts()
            {
                IdContent = 3,
                ContentName = "Burger",
                Quantity = 1
            };

            List<BasketsProducts> listBasketProd = new List<BasketsProducts>()
            {
                basketsProducts2,
                basketsProducts1,
                basketsProducts
            };

            Baskets content = new Baskets()
            {
                UserId = "user13",
                products = listBasketProd
            };

            var response = await Client.PostAsync("/Baskets", new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json"));


            //// Assert   
            Assert.IsTrue(response.IsSuccessStatusCode, "Impossible de Delete");
        }

        [TestMethod]
        public async Task DeleteBaskets_ReturnsOkResult()
        {

            var response = await Client.DeleteAsync("/Baskets/user13");

            // Assert   
            Assert.IsTrue(response.IsSuccessStatusCode, "Impossible de Delete");
        }
    }

}