﻿
using BLL.Interfaces;
using BLL.Services;
using DAL;
using DAL.Interfaces;
using DAL.Operation;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using StackExchange.Redis;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace order_api;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddCors(options =>
        {
            options.AddPolicy("AllowAll",
                builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyMethod()
                           .AllowAnyHeader();
                });
        });


        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

        //POSTGRES_USERNAME: root
        //  POSTGRES_PASSWORD: Password2023!
        //      POSTGRES_HOST: db
        //      POSTGRES_PORT: 5432
        //      POSTGRES_DATABASE: order
        string? connectionStringPostgres = "";

        ConfigurationOptions configRedis;


        var configuration = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json")
          .Build();


        builder.Services.AddHttpClient("Catalog-api", client =>
        {
            string connectionString = "";
            if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
            {
                connectionString = configuration.GetConnectionString("CatalogApiUrl") ?? "https://localhost:44382/";
            }
            else
            {
                connectionString = Environment.GetEnvironmentVariable("CatalogApiUrl") ?? configuration.GetConnectionString("CatalogApiUrl");
            }
            client.BaseAddress = new Uri(connectionString);
        });

        builder.Services.AddHttpClient("Restaurant-api", client =>
        {
            string connectionString = "";
            if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
            {
                connectionString = configuration.GetConnectionString("RestaurantApiUrl") ?? "https://localhost:44382/";
            }
            else
            {
                connectionString = Environment.GetEnvironmentVariable("RestaurantApiUrl") ?? configuration.GetConnectionString("RestaurantApiUrl");
            }
            client.BaseAddress = new Uri(connectionString);
        });

        builder.Services.AddHttpClient("Delivery-api", client =>
        {
            string connectionString = "";
            if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
            {
                connectionString = configuration.GetConnectionString("DeliveryApiUrl") ?? "https://localhost:44382/";
            }
            else
            {
                connectionString = Environment.GetEnvironmentVariable("DeliveryApiUrl") ?? configuration.GetConnectionString("DeliveryApiUrl");
            }
            client.BaseAddress = new Uri(connectionString);
        });

        if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
        {
            string? POSTGRES_USERNAME = Environment.GetEnvironmentVariable("POSTGRES_USERNAME");
            string? POSTGRES_PASSWORD = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");
            string? POSTGRES_HOST = Environment.GetEnvironmentVariable("POSTGRES_HOST");
            string? POSTGRES_PORT = Environment.GetEnvironmentVariable("POSTGRES_PORT");
            string? POSTGRES_DATABASE = Environment.GetEnvironmentVariable("POSTGRES_DATABASE");

            connectionStringPostgres = $"Host={POSTGRES_HOST};Port={POSTGRES_PORT};Database={POSTGRES_DATABASE};Username={POSTGRES_USERNAME};Password={POSTGRES_PASSWORD}";

            string? REDIS_HOST = Environment.GetEnvironmentVariable("REDIS_HOST");
            string? REDIS_PORT = Environment.GetEnvironmentVariable("REDIS_PORT");
            string? REDIS_PASSWORD = Environment.GetEnvironmentVariable("REDIS_PASSWORD");

            configRedis = new ConfigurationOptions
            {
                EndPoints = { $"{REDIS_HOST}:{REDIS_PORT}" },
                Password = REDIS_PASSWORD
            };
        }
        else
        {

            connectionStringPostgres = configuration.GetConnectionString("DefaultConnection");

            configRedis = new ConfigurationOptions
            {
                EndPoints = { "localhost:6379" }
            };
        }

        if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")) || Environment.GetEnvironmentVariable("ENV")!.ToUpper() == "TEST")
        {
            builder.Services.AddDbContext<OrdersDbContext>(options => options.UseInMemoryDatabase("TestDatabase"));
        }
        else
        {
            builder.Services.AddDbContext<OrdersDbContext>(options => options.UseNpgsql(connectionStringPostgres));
        }

        // Ajoutez ces lignes pour initialiser la base de donn�es
        using (var scope = builder.Services.BuildServiceProvider().CreateScope())
        {
            var context = scope.ServiceProvider.GetRequiredService<OrdersDbContext>();

            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")) && Environment.GetEnvironmentVariable("ENV")!.ToUpper() != "TEST")
            {
                context.Database.Migrate();
            }
        }

        builder.Services.AddScoped<IOrderContentsService, OrderContentsService>();
        builder.Services.AddScoped<IOrdersService, OrdersService>();
        builder.Services.AddScoped<IBasketsServices, BasketsServices>();

        builder.Services.AddScoped<IOrderContentsOperation, OrdersContentsOperation>();
        builder.Services.AddScoped<IOrdersOperation, OrdersOperation>();
        builder.Services.AddScoped<IBasketsOperation, BasketsOperation>();

        builder.Services.AddScoped<IDatabase>(cfg =>
        {
            IConnectionMultiplexer multiplexer = ConnectionMultiplexer.Connect(configRedis);
            return multiplexer.GetDatabase();
        });


        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "API Order", Version = "v1" });

            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ENV")))
            {
                c.DocumentFilter<SwaggerDocumentFilter>();
            }
        });

        var app = builder.Build();

        app.UseCors("AllowAll");

        // Configure the HTTP request pipeline.
        if (Environment.GetEnvironmentVariable("ENV") != "PROD")
        {
            app.UseSwagger();
            app.UseSwaggerUI();

            Console.WriteLine("Environnement : " + Environment.GetEnvironmentVariable("ENV"));
            Console.WriteLine(connectionStringPostgres);
            Console.WriteLine(configRedis);
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();

        app.MapControllers();

        app.Run();

    }

    public class SwaggerDocumentFilter : IDocumentFilter
    {
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            // Pr�fixe � ajouter aux chemins des appels HTTP
            string pathPrefix = "/order";

            var modifiedPaths = new Dictionary<string, OpenApiPathItem>();

            foreach (var pathItem in swaggerDoc.Paths)
            {
                var newPathItem = new OpenApiPathItem();

                foreach (var operation in pathItem.Value.Operations)
                {
                    newPathItem.AddOperation(operation.Key, operation.Value);
                }

                modifiedPaths[pathPrefix + pathItem.Key] = newPathItem;
            }

            // Ajouter les chemins modifi�s au document Swagger
            foreach (var modifiedPath in modifiedPaths)
            {
                swaggerDoc.Paths[modifiedPath.Key] = modifiedPath.Value;
            }

            // Supprimer les anciens chemins
            foreach (var pathItemKey in modifiedPaths.Keys)
            {
                swaggerDoc.Paths.Remove(pathItemKey.Substring(pathPrefix.Length));
            }
        }
    }
}
