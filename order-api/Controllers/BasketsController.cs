using BLL.Interfaces;
using DAL.Entity;
using Microsoft.AspNetCore.Mvc;

namespace order_api.Controllers
{
    [ApiController]
    [Route("baskets")]
    public class BasketsController : ControllerBase
    {
        private readonly IBasketsServices _basketsService;

        public BasketsController(IBasketsServices BasketsService)
        {
            _basketsService = BasketsService;
        }

        // GET: api/Baskets
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Baskets>>> GetBaskets()
        {
            var deliveries = await _basketsService.GetBasketsAsync();
            return Ok(deliveries);
        }

        // GET: api/Baskets/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Baskets>> GetBaskets(string id)
        {
            var deliveries = await _basketsService.GetBasketsByUserIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // POST: api/Baskets
        [HttpPost]
        public async Task<ActionResult<Baskets>> PostBaskets(Baskets Baskets)
        {
            if (Baskets.products == null)
            {
                return BadRequest();
            }
            await _basketsService.CreateBasketsAsync(Baskets);

            return CreatedAtAction("GetBaskets", new { id = Baskets.UserId }, Baskets);
        }

        // POST: api/Baskets
        [HttpPost("validate/{id}")]
        public async Task<ActionResult<Orders>> Validate(string id, Orders myOrder)
        {
            if (id == "" && String.IsNullOrEmpty(myOrder.Address) && String.IsNullOrEmpty(myOrder.Country) &&
                String.IsNullOrEmpty(myOrder.City) &&
                 String.IsNullOrEmpty(myOrder.ZipCode) &&
                !(myOrder.CommandType == "Fournisseur" || myOrder.CommandType == "Client"))
            {
                return BadRequest("if doesn't pass");
            }
            try
            {
                myOrder = await _basketsService.Validate(myOrder, id);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
                throw;
            }

            return Ok(myOrder);
        }

        // Put: api/Baskets
        [HttpPut("{id}")]
        public async Task<ActionResult<Baskets>> PutBaskets(string id, Baskets baskets)
        {
            if (id == "" && id == null)
            {
                return BadRequest();
            }

            await _basketsService.UpdateBasketsAsync(id, baskets);

            return NoContent();
        }

        // DELETE: api/Baskets/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBaskets(string id)
        {
            var deliveries = await _basketsService.GetBasketsByUserIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            await _basketsService.DeleteBasketsAsync(id);

            return NoContent();
        }
    }
}