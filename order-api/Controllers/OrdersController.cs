using BLL.Interfaces;
using DAL.Entity;
using Microsoft.AspNetCore.Mvc;

namespace order_api.Controllers
{
    [ApiController]
    [Route("orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        // GET: api/Orders
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<Orders>>> GetOrders()
        {
            var orders = await _ordersService.GetOrdersAsync();
            return Ok(orders);
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Orders>> GetOrders(int id)
        {
            var orders = await _ordersService.GetOrdersByIdAsync(id);
            if (orders == null)
            {
                return NotFound();
            }

            return Ok(orders);
        }

        // GET: api/GetOrderBestContents/5
        [HttpGet("restaurant/{IdRestaurant}/count")]
        public async Task<ActionResult<CountOrders>> CountOrders(int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var deliveries = await _ordersService.CountOrders(IdRestaurant, dateMin, dateMax);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/Orders
        [HttpGet("user")]
        public async Task<ActionResult<IEnumerable<Orders>>> GetUserOrders(string Email)
        {
            var orders = await _ordersService.GetUserOrdersAsync(Email);
            if (orders == null)
            {
                return NotFound();
            }
            return Ok(orders);
        }

        // POST: api/Orders
        [HttpPost()]
        public async Task<ActionResult<Orders>> CreateOrder(Orders Orders)
        {
            await _ordersService.CreateOrdersAsync(Orders);

            return CreatedAtAction("GetOrders", new { id = Orders.Id }, Orders);
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            var orders = await _ordersService.GetOrdersByIdAsync(id);
            if (orders == null)
            {
                return NotFound();
            }

            await _ordersService.DeleteOrdersAsync(id);

            return NoContent();
        }
    }
}