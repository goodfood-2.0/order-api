using BLL.Interfaces;
using DAL.Entity;
using Microsoft.AspNetCore.Mvc;

namespace order_api.Controllers
{
    [ApiController]
    [Route("ordercontents")]
    public class OrderContentsController : ControllerBase
    {
        private readonly IOrderContentsService _orderContentsService;

        public OrderContentsController(IOrderContentsService OrderContentsService)
        {
            _orderContentsService = OrderContentsService;
        }

        // GET: api/OrderContents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderContents>>> GetOrderContents()
        {
            var deliveries = await _orderContentsService.GetOrderContentsAsync();
            return Ok(deliveries);
        }

        // GET: api/OrderContents/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OrderContents>> GetOrderContents(int id)
        {
            var deliveries = await _orderContentsService.GetOrderContentsByIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/orders/bestcontents/5
        [HttpGet("bestcontents")]
        public async Task<ActionResult<IEnumerable<TopBestContent>>> GetOrderBestContents(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var deliveries = await _orderContentsService.GetOrderBestContents(NumberOfBest, IdRestaurant, dateMin, dateMax);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/GetOrderBestSold
        [HttpGet("bestsold")]
        public async Task<ActionResult<IEnumerable<TopSoldContent>>> GetOrderBestSold(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var deliveries = await _orderContentsService.GetOrderBestSold(NumberOfBest, IdRestaurant, dateMin, dateMax);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // GET: api/GetTotalAmount/5
        [HttpGet("totalamount")]
        public async Task<ActionResult<TotalAmount>> GetTotalAmount(int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var deliveries = await _orderContentsService.GetTotalAmount(IdRestaurant, dateMin, dateMax);
            if (deliveries == null)
            {
                return NotFound();
            }

            return Ok(deliveries);
        }

        // POST: api/OrderContents
        [HttpPost]
        public async Task<ActionResult<OrderContents>> PostOrderContents(OrderContents OrderContents)
        {
            await _orderContentsService.CreateOrderContentsAsync(OrderContents);

            return CreatedAtAction("GetOrderContents", new { id = OrderContents.Id }, OrderContents);
        }

        // PUT: api/OrderContents/5
        [HttpPut("{id}")]
        public async Task<ActionResult<OrderContents>> PutOrderContents(int id, OrderContents OrderContents)
        {
            if (id != OrderContents.Id)
            {
                return BadRequest();
            }

            await _orderContentsService.UpdateOrderContentsAsync(id, OrderContents);


            return Ok(await _orderContentsService.GetOrderContentsByIdAsync(OrderContents.Id));
        }

        // DELETE: api/OrderContents/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderContents(int id)
        {
            var deliveries = await _orderContentsService.GetOrderContentsByIdAsync(id);
            if (deliveries == null)
            {
                return NotFound();
            }

            await _orderContentsService.DeleteOrderContentsAsync(id);

            return NoContent();
        }
    }
}