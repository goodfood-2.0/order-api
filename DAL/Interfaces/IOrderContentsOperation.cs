﻿using DAL.Entity;

namespace DAL.Interfaces
{
    public interface IOrderContentsOperation
    {
        public Task<IEnumerable<OrderContents>> GetOrderContentsAsync();
        public Task<OrderContents> GetOrderContentsByIdAsync(int id);
        public Task CreateOrderContentsAsync(OrderContents Orders);
        public Task UpdateOrderContentsAsync(int id, OrderContents Orders);
        public Task DeleteOrderContentsAsync(int id);
        public Task<IEnumerable<TopBestContent>> GetOrderBestContents(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<IEnumerable<TopSoldContent>> GetOrderBestSold(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax);
        public Task<TotalAmount> GetTotalAmount(int IdRestaurant, DateTime dateMin, DateTime dateMax);
    }

}
