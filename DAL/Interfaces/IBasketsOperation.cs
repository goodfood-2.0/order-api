﻿using DAL.Entity;

namespace DAL.Interfaces
{
    public interface IBasketsOperation
    {
        Task CreateBasketsAsync(Baskets ordersContent);
        Task DeleteBasketsAsync(string id);
        Task<IEnumerable<Baskets>> GetBasketsAsync();
        Task<Baskets> GetBasketsByUserIdAsync(string id);
        Task UpdateBasketsAsync(string id, Baskets baskets);
    }
}
