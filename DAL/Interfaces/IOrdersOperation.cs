﻿using DAL.Entity;

namespace DAL.Interfaces
{
    public interface IOrdersOperation
    {
        public Task<IEnumerable<Orders>> GetOrdersAsync();
        public Task<Orders> GetOrdersByIdAsync(int id);
        public Task<Orders> CreateOrdersAsync(Orders Orders);
        public Task UpdateOrdersAsync(int id, Orders Orders);
        public Task DeleteOrdersAsync(int id);
        Task<IEnumerable<Orders>> GetUserOrdersAsync(string email);
        Task<CountOrders> CountOrders(int IdRestaurant, DateTime dateMin, DateTime dateMax);
    }

}
