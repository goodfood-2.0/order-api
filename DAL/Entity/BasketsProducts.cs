﻿namespace DAL.Entity
{
    public class BasketsProducts
    {
        public int IdContent { get; set; }
        public string ContentName { get; set; } = "";
        public int Quantity { get; set; }
    }
}
