﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entity
{
    public class Baskets
    {
        [Key]
        public string? UserId { get; set; }
        public List<BasketsProducts>? products { get; set; }

    }
}
