﻿namespace DAL.Entity
{
    public class OrderContents
    {
        public int Id { get; set; }
        public int IdOrder { get; set; }
        public int IdContent { get; set; }
        public string ContentName { get; set; } = "";
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
