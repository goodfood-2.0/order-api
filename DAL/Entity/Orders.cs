﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entity
{
    public class Orders
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public int IdRestaurant { get; set; }
        public string Country { get; set; } = string.Empty;
        public string City { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string AdditionnalAddress { get; set; } = string.Empty;
        public string ZipCode { get; set; } = string.Empty;
        public string CommandType { get; set; } = string.Empty;

        public bool isValidate { get; set; } = false;

        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public List<OrderContents> OrderContents { get; set; } = new List<OrderContents>();
    }
}
