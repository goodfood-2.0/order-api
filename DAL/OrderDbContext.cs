﻿using DAL.Entity;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class OrdersDbContext : DbContext
    {

        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderContents> OrderContents { get; set; }

        public OrdersDbContext(DbContextOptions<OrdersDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Orders>().HasKey(x => x.Id);

            modelBuilder.Entity<Orders>()
                .HasMany(x => x.OrderContents)
                .WithOne()
                .HasForeignKey(x => x.IdOrder);


            modelBuilder.Entity<OrderContents>().HasKey(x => x.Id);
            // configurez d'autres propriétés de votre modèle ici si nécessaire
        }
    }
}
