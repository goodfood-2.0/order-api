﻿namespace DAL.Entity
{
    public class TopBestContent
    {
        public TopBestContent(int idRestaurant, int idContent, int salesCount, DateTime dateMin, DateTime dateMax)
        {
            IdRestaurant = idRestaurant;
            IdContent = idContent;
            SalesCount = salesCount;
            DateMin = dateMin;
            DateMax = dateMax;
        }
        public int IdRestaurant { get; set; }
        public int IdContent { get; set; }
        public int SalesCount { get; set; }

        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
