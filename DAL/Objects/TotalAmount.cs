﻿namespace DAL.Entity
{
    public class TotalAmount
    {
        public TotalAmount()
        {
        }
        public int IdRestaurant { get; set; }
        public double Total { get; set; }
        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
