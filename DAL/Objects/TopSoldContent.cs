﻿namespace DAL.Entity
{
    public class TopSoldContent
    {
        public TopSoldContent(int idRestaurant, int idContent, double totalPrice, DateTime dateMin, DateTime dateMax)
        {
            IdRestaurant = idRestaurant;
            IdContent = idContent;
            TotalPrice = totalPrice;
            DateMin = dateMin;
            DateMax = dateMax;
        }
        public int IdRestaurant { get; set; }
        public int IdContent { get; set; }
        public double TotalPrice { get; set; }
        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
