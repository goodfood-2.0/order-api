﻿namespace DAL.Entity
{
    public class CountOrders
    {
        public CountOrders()
        {
        }

        public int IdRestaurant { get; set; }
        public double Count { get; set; }
        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
