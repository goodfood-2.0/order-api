﻿using DAL.Entity;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Operation
{
    public class OrdersOperation : IOrdersOperation
    {
        private readonly OrdersDbContext _context;

        public OrdersOperation(OrdersDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Orders>> GetOrdersAsync()
        {
            return await _context.Orders.Include(x => x.OrderContents).ToListAsync();
        }

        public async Task<Orders> GetOrdersByIdAsync(int id)
        {
            return await _context.Orders.Include(x => x.OrderContents).Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Orders>> GetUserOrdersAsync(string email)
        {
            return await _context.Orders
                .Include(x => x.OrderContents)
                .Where(x => x.Email == email).ToListAsync();
        }


        public async Task<CountOrders> CountOrders(int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            CountOrders res = new CountOrders();

            res.IdRestaurant = IdRestaurant;
            res.DateMin = dateMin;
            res.DateMax = dateMax;
            res.Count = _context.Orders
                .Where(order => IdRestaurant != 0 ? order.IdRestaurant == IdRestaurant : true)
                .Where(order => order.CommandType != "Fournisseur")
                .Where(order => order.UpdatedAt >= dateMin && order.UpdatedAt <= dateMax)
                .Count();

            return res;

        }

        public async Task<Orders> CreateOrdersAsync(Orders Orders)
        {
            _context.Orders.Add(Orders);
            await _context.SaveChangesAsync();
            return Orders;
        }

        public async Task UpdateOrdersAsync(int id, Orders updatedOrder)
        {
            var existingOrders = await _context.Orders.FindAsync(id);

            if (existingOrders != null)
            {
                existingOrders.Country = updatedOrder.Country;
                existingOrders.City = updatedOrder.City;
                existingOrders.Address = updatedOrder.Address;
                existingOrders.AdditionnalAddress = updatedOrder.AdditionnalAddress;
                existingOrders.ZipCode = updatedOrder.ZipCode;
                existingOrders.CommandType = updatedOrder.CommandType;
                existingOrders.isValidate = updatedOrder.isValidate;

                // Mettez à jour la date de mise à jour
                existingOrders.UpdatedAt = DateTime.UtcNow;

                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteOrdersAsync(int id)
        {
            var existingOrders = await _context.Orders.FindAsync(id);

            if (existingOrders != null)
            {
                _context.Orders.Remove(existingOrders);
                await _context.SaveChangesAsync();
            }
        }
    }
}

