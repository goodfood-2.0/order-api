﻿using DAL.Entity;
using DAL.Interfaces;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace DAL.Operation
{
    public class BasketsOperation : IBasketsOperation
    {

        private readonly IDatabase _context;

        public BasketsOperation(IDatabase context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Baskets>> GetBasketsAsync()
        {
            List<Baskets> lBaskets = new List<Baskets>();

            var res = _context.Multiplexer
            .GetServer(_context.Multiplexer
                .GetEndPoints()
                .First())
            .Keys(_context.Database, $"{typeof(Baskets)}*")
            .Select(x => x.ToString())
            .ToList();

            res.ForEach(x =>
            {
                var serializedBaskets = _context.StringGet(x);

                var result = JsonConvert.DeserializeObject<Baskets>(serializedBaskets);
                lBaskets.Add(result);
            });

            return lBaskets;
        }

        public async Task<Baskets> GetBasketsByUserIdAsync(string id)
        {
            var serializedBaskets = await _context.StringGetAsync($"{typeof(Baskets)}:{id}");
            if (serializedBaskets.IsNull)
            {
                return null;
            }
            return JsonConvert.DeserializeObject<Baskets>(serializedBaskets);
        }

        public async Task CreateBasketsAsync(Baskets baskets)
        {
            //long id = await _context.StringIncrementAsync("idIncrement");
            var key = $"{typeof(Baskets)}:{baskets.UserId}";
            var serializedBaskets = JsonConvert.SerializeObject(baskets);

            await _context.StringSetAsync(key, serializedBaskets);
        }

        public async Task DeleteBasketsAsync(string id)
        {
            await _context.KeyDeleteAsync($"{typeof(Baskets)}:{id}");
        }

        public async Task UpdateBasketsAsync(string id, Baskets baskets)
        {
            await _context.KeyDeleteAsync($"{typeof(Baskets)}:{id}");

            var serializedBaskets = JsonConvert.SerializeObject(baskets);
            await _context.StringSetAsync($"{typeof(Baskets)}:{id}", serializedBaskets);
        }
    }
}

