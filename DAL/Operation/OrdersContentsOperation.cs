﻿using DAL.Entity;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Operation
{
    public class OrdersContentsOperation : IOrderContentsOperation
    {
        private readonly OrdersDbContext _context;

        public OrdersContentsOperation(OrdersDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<OrderContents>> GetOrderContentsAsync()
        {
            return await _context.OrderContents.ToListAsync();
        }

        public async Task<OrderContents> GetOrderContentsByIdAsync(int id)
        {
            return await _context.OrderContents.FindAsync(id);
        }

        public async Task<IEnumerable<TopBestContent>> GetOrderBestContents(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var topContentSales = _context.Orders
                .Where(order => IdRestaurant != 0 ? order.IdRestaurant == IdRestaurant : true)
                .Where(order => order.CommandType != "Fournisseur")
                .Where(order => order.UpdatedAt >= dateMin && order.UpdatedAt <= dateMax)
                .SelectMany(order => order.OrderContents)
                .GroupBy(orderContent => orderContent.IdContent)
                .OrderByDescending(contentGroup => contentGroup.Sum(content => content.Quantity))
                .Take(NumberOfBest)
                .Select(contentGroup => new TopBestContent(IdRestaurant, contentGroup.Key, contentGroup.Sum(content => content.Quantity), dateMin, dateMax))
                .ToList();

            return topContentSales;

        }

        public async Task<IEnumerable<TopSoldContent>> GetOrderBestSold(int NumberOfBest, int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            var topContentSales = _context.Orders
                .Where(order => IdRestaurant != 0 ? order.IdRestaurant == IdRestaurant : true)
                .Where(order => order.CommandType != "Fournisseur")
                .Where(order => order.UpdatedAt >= dateMin && order.UpdatedAt <= dateMax)
                .SelectMany(order => order.OrderContents)
                .GroupBy(orderContent => orderContent.IdContent)
                .OrderByDescending(contentGroup => contentGroup.Sum(content => content.Quantity * content.Price))
                .Take(NumberOfBest)
                .Select(contentGroup => new TopSoldContent(IdRestaurant, contentGroup.Key, (double)contentGroup.Sum(content => content.Quantity * content.Price), dateMin, dateMax))
                .ToList();

            return topContentSales;

        }

        public async Task<TotalAmount> GetTotalAmount(int IdRestaurant, DateTime dateMin, DateTime dateMax)
        {
            TotalAmount res = new TotalAmount();

            res.IdRestaurant = IdRestaurant;
            res.DateMin = dateMin;
            res.DateMax = dateMax;
            res.Total = (double)_context.Orders
                .Where(order => IdRestaurant != 0 ? order.IdRestaurant == IdRestaurant : true)
                .Where(order => order.CommandType != "Fournisseur")
                .Where(order => order.UpdatedAt >= dateMin && order.UpdatedAt <= dateMax)
                .Sum(order => order.OrderContents.Sum(content => content.Quantity * content.Price));

            return res;
        }

        public async Task CreateOrderContentsAsync(OrderContents ordersContent)
        {
            _context.OrderContents.Add(ordersContent);
            await _context.SaveChangesAsync();
        }


        public async Task UpdateOrderContentsAsync(int id, OrderContents ordersContent)
        {

            var existingOrders = await _context.OrderContents.FindAsync(id);

            if (existingOrders != null)
            {
                existingOrders.IdContent = ordersContent.IdContent;
                existingOrders.Quantity = ordersContent.Quantity;
                existingOrders.Price = ordersContent.Price;

                await _context.SaveChangesAsync();
            }

        }

        public async Task DeleteOrderContentsAsync(int id)
        {
            var existingOrders = await _context.OrderContents.FindAsync(id);

            if (existingOrders != null)
            {
                _context.OrderContents.Remove(existingOrders);
                await _context.SaveChangesAsync();
            }
        }


    }

}

